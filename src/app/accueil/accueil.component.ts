import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {

  listArticle = [
      {titre: "Titre 1", categorie: "Cine"},
      {titre: "Titre 2", categorie: "Cine"},
      {titre: "Titre 3", categorie: "Cine"},
      {titre: "Titre 4", categorie: "Musique"},
      {titre: "Titre 5", categorie: "Musique"},
      {titre: "Titre 6", categorie: "Musique"},
      {titre: "Titre 7", categorie: "Play"}
  ]

  listCategories: string[] = [];

  filter: string = null;

  constructor(private router: Router) { }

  ngOnInit() {
    console.log("filter -> ", this.filter);
    this.getCategorie();
  }

    logout() {
        this.router.navigate(['/login']);
    }

    filterBy(value){
    console.log(value);
    this.filter = value;
    }

    getCategorie(){

      this.listArticle.forEach(article => {
        console.log(article.categorie);
        if(! this.listCategories.some((item) => item == article.categorie)){
          this.listCategories.push(article.categorie);
        }
      })

    }

}
