import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthGuard} from '../service/auth.guard';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private authService: AuthGuard, private router: Router) { }

  ngOnInit() {

    this.authService.logout();
    this.loginForm = this.formBuilder.group({
        username: [''],
        password: ['']
    });
  }

  get f() { return this.loginForm.controls; }

  onSubmit(){
    if (this.loginForm.invalid){
      console.log("invalid");
      return;
    }

    if (this.f.username.value == "toto" && this.f.password.value == "toto"){
        localStorage.setItem('currentUser', JSON.stringify({username: this.f.username.value, password: this.f.password.value}));
        this.router.navigate(['']);
    } else {
        console.log("Infos invalid");
      return;
    }
  }

}
