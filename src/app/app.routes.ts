///<reference path="home/home.component.ts"/>
import {Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from './service/auth.guard';
import {AccueilComponent} from './accueil/accueil.component';

export const ROUTES = [
    {
        path: '',
        component: HomeComponent,
        canActivateChild: [AuthGuard],
        children: [
            {path: '', pathMatch: 'full', redirectTo: 'home'},
            {path: 'home', component: AccueilComponent}
        ]},
    {path: 'login', component: LoginComponent },
    { path: '**', redirectTo: '' }
    ];